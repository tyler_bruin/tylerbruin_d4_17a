﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{

    public partial class InstructorTimetable : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        //SqlConnection con = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True");
     
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read1;

        public string user = "";

        public void stuff()
        {
            con.Open();
            cmd.CommandText = ($"select ID, Date, Time, InstructorName, Client, Status, Timeoff, ClientType from TIMESLOT WHERE InstructorName = '{user}'");
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();

            while (read1.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read1["ID"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["ClientType"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public void hidestuff()
        {
            listView1.Items.Clear();

            con.Open();
            cmd.CommandText = ($"select ID, Date, Time, InstructorName, Client, Status, Timeoff, ClientType from TIMESLOT WHERE InstructorName = '{user}' AND Timeoff IS NULL");
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();

            while (read1.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read1["ID"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["ClientType"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public InstructorTimetable(string currentuser)
        {
            InitializeComponent();

            user = currentuser;
            label9.Text = user;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void InstructorTimetable_Load(object sender, EventArgs e)
        {
            stuff();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                listView1.Items.Clear();
                hidestuff();
            }
            else
            {
                listView1.Items.Clear();
                stuff();
            }
        }

        private void button1_Click(object sender, EventArgs e) // DRIVEN BUTTON
        {
            DialogResult dialogResult = MessageBox.Show("Confirm this appointment Completed?", "Warning!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string Id = "";
                foreach (ListViewItem eachItem in listView1.SelectedItems)
                {
                    Id = listView1.SelectedItems[0].Text;
                    //Client = (listView1.GetItemAt.(Position4));
                    string Date = listView1.SelectedItems[0].SubItems[1].Text;
                    string Time = listView1.SelectedItems[0].SubItems[2].Text;
                    string Instructor = listView1.SelectedItems[0].SubItems[3].Text;
                    string Client = listView1.SelectedItems[0].SubItems[4].Text;
                    string ClientType = listView1.SelectedItems[0].SubItems[7].Text;
                    string Cost = "";
                    if (ClientType == "Beginner")
                    {
                        Cost = "$200";
                    }
                    if (ClientType == "Intermediate")
                    {
                        Cost = "$150";
                    }
                    if (ClientType == "Advanced")
                    {
                        Cost = "$100";
                    }
                    SQL.executeQuery($"UPDATE Timeslot SET Status = 'Completed' WHERE Id = '{Id}' AND Status IS NOT NULL"); //sql query
                    listView1.Items.Clear();
                    stuff();
                    checkBox1.Checked = false;
                    string text = ($"---- DIA Driving Academy ----\r\nDear {Client},\r\nSession Bill:\r\n{ClientType} session with {Instructor} at {Time} on {Date}.\r\nAmount due: {Cost}.\r\n\r\nThank you.");
                    System.IO.StreamWriter file = new System.IO.StreamWriter($"c:\\{Client}_{Date}_Invoice.txt");
                    file.WriteLine(text);
                    file.Close();
                }
                con.Close();
            }
        }
    }
}