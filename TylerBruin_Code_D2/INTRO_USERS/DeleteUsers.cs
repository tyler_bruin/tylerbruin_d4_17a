﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class DeleteUsers : Form
    {
        //SqlConnection con = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True");
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");

        SqlCommand cmd = new SqlCommand();
        SqlDataReader read;

        public DeleteUsers()
        {
            InitializeComponent();

        }

        public void loadUserTimetable()
        {
            userListView.Items.Clear();

            con.Open();
            cmd.CommandText = ($"SELECT username FROM Users WHERE Status = 'admin' OR Status = 'instructor'");
            cmd.Connection = con;
            read = cmd.ExecuteReader();

            while (read.Read())
            {
                userListView.View = View.Details;

                ListViewItem item = new ListViewItem(read["Username"].ToString());


                userListView.Items.Add(item);
            }
            con.Close();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            string username = "";
            string usernamea = "";
            string usernamei = "";


            foreach (ListViewItem eachItem in userListView.SelectedItems)
            {
                username = userListView.SelectedItems[0].Text;
                usernamea = userListView.SelectedItems[0].Text;
                usernamei = userListView.SelectedItems[0].Text;

                // label14.Text = carid; // to test if variable is passed through

                SQL.executeQuery($"Delete From Users Where username = '{username}'"); //sql query
                SQL.executeQuery($"Delete From Admin Where username = '{usernamea}'"); //sql query 
                SQL.executeQuery($"Delete From Instructor Where username = '{usernamei}'"); //sql query 
                userListView.Items.Remove(eachItem); // deletes item from listview


            }
        }


        private void DeleteUsers_Load(object sender, EventArgs e)
        {
            loadUserTimetable();
        }
    }
}
