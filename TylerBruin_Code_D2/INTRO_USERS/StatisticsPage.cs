﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class StatisticsPage : Form
    {

        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        //SqlConnection con = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True");

        SqlCommand cmd = new SqlCommand();
        SqlDataReader read;

        public StatisticsPage()
        {
            InitializeComponent();
        }

        public void loadStatistics()
        {

            // ----------------------------------------------------User Stats----------------------------------------------------------------- 
            con.Open();
            cmd.CommandText = ($"Select count(*) From Users");
            cmd.Connection = con;
            label1.Text = "Total Registered Users: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Admin");
            cmd.Connection = con;
            label2.Text = "Total Administrators: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Instructor Where Firstname NOT LIKE 'Spare%'");
            cmd.Connection = con;
            label3.Text = "Total Instructors: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client");
            cmd.Connection = con;
            label4.Text = "Total Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client where Type = 'Beginner'");
            cmd.Connection = con;
            label6.Text = "Total Beginner Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client where Type = 'Intermediate'");
            cmd.Connection = con;
            label7.Text = "Total Intermediate Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Client where Type = 'Advanced'");
            cmd.Connection = con;
            label8.Text = "Total Advanced Clients: " + cmd.ExecuteScalar().ToString();
            con.Close();

            // ----------------------------------------------------Appointment Stats----------------------------------------------------------------- 

            con.Open();
            cmd.CommandText = ($"Select count(*) From Timeslot Where Client != 'Blocked Out' ");
            cmd.Connection = con;
            label5.Text = "Total Appointments: " + cmd.ExecuteScalar().ToString();
            con.Close();

            con.Open();
            cmd.CommandText = ($"Select count(*) From Timeslot Where timeoff = 'Requested'");
            cmd.Connection = con;
            label9.Text = "Total pending Instructor block out requests: " + cmd.ExecuteScalar().ToString();
            con.Close();
   
            con.Open();
            cmd.CommandText = ($"Select count(*) from timeslot where date like '%June%' and Client != 'Blocked Out'");
            cmd.Connection = con;
            label10.Text = "Total appointments this month: " + cmd.ExecuteScalar().ToString();
            con.Close();

            //
            //
        }
        public void loadAppointmentsTimetable()
        {
            adminCarListView1.Items.Clear();

            con.Open();
            cmd.CommandText = ($"SELECT count(InstructorName) As Total, InstructorName From Timeslot Where Client != 'Blocked Out' Group By InstructorName ");
            cmd.Connection = con;
            read = cmd.ExecuteReader();

            while (read.Read())
            {
                adminCarListView1.View = View.Details;

                ListViewItem item = new ListViewItem(read["Total"].ToString());
                item.SubItems.Add(read["InstructorName"].ToString());


                adminCarListView1.Items.Add(item);
            }
            con.Close();
        }

        private void StatisticsPage_Load(object sender, EventArgs e)
        {
            loadStatistics();
            loadAppointmentsTimetable();
        }
    }
}
