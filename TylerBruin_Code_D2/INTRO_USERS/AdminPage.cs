﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class AdminPage : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        //SqlConnection con = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True");

        SqlCommand cmd = new SqlCommand();
        SqlDataReader read1;
        SqlDataReader read2;
        SqlDataReader read3;
        SqlDataReader read4;
        SqlDataReader read5;
        SqlDataReader read6;

        public string currentuser = "";

        

        
         public void loadAdminTimetable()
        {
            adminListView.Items.Clear();

            con.Open();
            cmd.CommandText = ($"SELECT Id, Date, Time, InstructorName, Client, Status, Timeoff, ClientType from TIMESLOT");
            cmd.Connection = con;
            read2 = cmd.ExecuteReader();

            while (read2.Read())
            {
                adminListView.View = View.Details;

                ListViewItem item = new ListViewItem(read2["Id"].ToString());
                item.SubItems.Add(read2["Date"].ToString());
                item.SubItems.Add(read2["Time"].ToString());
                item.SubItems.Add(read2["InstructorName"].ToString());
                item.SubItems.Add(read2["Client"].ToString());
                item.SubItems.Add(read2["Status"].ToString());
                item.SubItems.Add(read2["Timeoff"].ToString());
                item.SubItems.Add(read2["ClientType"].ToString());

                adminListView.Items.Add(item);
            }
            con.Close();
        }
        public void loadCarTimetable()
        {
            adminCarListView1.Items.Clear();

            con.Open();
            cmd.CommandText = ($"SELECT License, InstructorName FROM Assigned");
            cmd.Connection = con;
            read5 = cmd.ExecuteReader();

            while (read5.Read())
            {
                adminCarListView1.View = View.Details;

                ListViewItem item = new ListViewItem(read5["License"].ToString());
                item.SubItems.Add(read5["InstructorName"].ToString());


                adminCarListView1.Items.Add(item);
            }
            con.Close();
        } 

        public void populateCarComboboxes()
        {

            con.Open();
            cmd.CommandText = ($"SELECT License FROM Car");
            cmd.Connection = con;
            read3 = cmd.ExecuteReader();

            while (read3.Read())
            {
                comboCar.Items.Add(read3["License"].ToString());
            }
            con.Close();
        }
        public void populateInstructorComboboxes()
        {
        /*    con.Open();
            SqlCommand sqlCmd1 = new SqlCommand($"SELECT FirstName, LastName FROM Instructor");
            SqlDataReader sqlReader = sqlCmd1.ExecuteReader();
             */
            con.Open();
            cmd.CommandText = ($"SELECT FirstName, LastName FROM Instructor");
            cmd.Connection = con;
            read4 = cmd.ExecuteReader();

            // name = SQL.read[2].ToString() + SQL.read[3].ToString();

            while (read4.Read())
            {
                comboInstructor.Items.Add(read4["FirstName"].ToString() + " " + (read4["LastName"].ToString()));
            }
            con.Close();
        }

        public void hidestuff()
        {
            //cmd.CommandText = ($"select Date, Time, InstructorName, Client from TIMESLOT WHERE InstructorName = '{user}' AND NOT Client = 'Blocked Out'");

            adminListView.Items.Clear();

            con.Open();
            cmd.CommandText = ($"select ID, Date, Time, InstructorName, Client, Status, Timeoff, ClientType from TIMESLOT WHERE InstructorName = '{currentuser}' AND Timeoff IS NULL");
            cmd.Connection = con;
            read6 = cmd.ExecuteReader();

            while (read6.Read())
            {
                adminListView.View = View.Details;

                ListViewItem item = new ListViewItem(read6["Id"].ToString());
                item.SubItems.Add(read6["Date"].ToString());
                item.SubItems.Add(read6["Time"].ToString());
                item.SubItems.Add(read6["InstructorName"].ToString());
                item.SubItems.Add(read6["Client"].ToString());
                item.SubItems.Add(read6["Status"].ToString());
                item.SubItems.Add(read6["Timeoff"].ToString());
                item.SubItems.Add(read6["ClientType"].ToString());

                adminListView.Items.Add(item);
            }
            con.Close();

        }
        public void filter() //FILTER -----------------------------------------------
        {
            con.Open();
            SqlCommand sqlCmd3 = new SqlCommand("SELECT FirstName, LastName FROM Instructor", con);
            SqlDataReader sqlReader3 = sqlCmd3.ExecuteReader();
            while (sqlReader3.Read())
            {
                comboBoxFilterInstructor.Items.Add(sqlReader3["FirstName"].ToString() + " " + (sqlReader3["LastName"].ToString()));
            }
            sqlReader3.Close();

            SqlCommand sqlCmd4 = new SqlCommand("SELECT name FROM Client", con);
            SqlDataReader sqlReader4 = sqlCmd4.ExecuteReader();
            while (sqlReader4.Read())
            {
                comboBoxFilterClient.Items.Add(sqlReader4["name"].ToString());
            }
            sqlReader4.Close();
            con.Close();

        }

        public AdminPage(string CurrentUser)
        {
            InitializeComponent();

            currentuser = CurrentUser;
            label9.Text = CurrentUser;
        }
    

        private void buttonBooking_Click(object sender, EventArgs e)
        {
          //  Hide();
            AdminBooking browsePage = new AdminBooking(currentuser);
            browsePage.ShowDialog();
         //   Close();
        }

        private void buttonBlocked_Click(object sender, EventArgs e)
        {
            AdminBlocking browsePage = new AdminBlocking(currentuser);
            browsePage.ShowDialog();
        }

        private void AdminPage_Load(object sender, EventArgs e)
        {
            loadAdminTimetable();
            loadCarTimetable();

            populateInstructorComboboxes();
            populateCarComboboxes();

            filter();
        }

        private void button1_Click(object sender, EventArgs e) // ------------------------- ASSIGN CARS TO INSTRUCTORS --------------------------------------
        {

            string InstructorName = "", License = "";
            InstructorName = comboInstructor.Text;
            License = comboCar.Text;

            string InstructorNameI = "", LicenseI = "";
            InstructorNameI = InstructorName;
            LicenseI = License;

            if (comboCar.SelectedItem == null || comboInstructor.Text == null)
            {
                MessageBox.Show("Error, Missing instructor or car.");
            }
            else
            {
                using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))              
                //using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True"))
                {

                    SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Assigned WHERE License = '{LicenseI}'", sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        if (sqlReader.HasRows)
                        {
                            LicenseI = sqlReader.GetString(0);
                            // InstructorNameI = sqlReader.GetString(1);
                            if (InstructorNameI == "" && LicenseI == "")
                            {
                                SQL.executeQuery("INSERT INTO Assigned (License, InstructorName) VALUES ( '" + License + "', '" + InstructorName + "')");
                                loadCarTimetable();
                                MessageBox.Show("Assigned Vehicle " + License + " To " + InstructorName);
                            }
                            else
                            {
                                MessageBox.Show("Vehicle unavalible. Please Pick Another, or delete exisiting assignment.");
                                return;
                            }
                        }
                    }
                    sqlReader.Close();
                }
                try
                {
                    SQL.executeQuery("INSERT INTO Assigned (License, InstructorName) VALUES ('" + License + "', '" + InstructorName + "')");
                    loadCarTimetable();
                    MessageBox.Show("You have assigned vehicle " + License + " To " + InstructorName);
                }
                catch (Exception)
                {
                    MessageBox.Show("Car already assigned.");
                    return;
                }
            }

        }

        public void button3_Click(object sender, EventArgs e) // ------------------------- DELETE Car Assignment -----------------------
        {
            string carid = "";


            foreach (ListViewItem eachItem in adminCarListView1.SelectedItems)
            {
                carid = adminCarListView1.SelectedItems[0].Text;
               // label14.Text = carid; // to test if variable is passed through

                SQL.executeQuery($"Delete From Assigned Where License = '{carid}' "); //sql query
                adminCarListView1.Items.Remove(eachItem); // deletes item from listview

            }
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e) // show/hide blocked out dates
        {
            if (checkBox1.Checked)
            {
                adminListView.Items.Clear();
                hidestuff();
            }
            else
            {
                adminListView.Items.Clear();
                loadAdminTimetable();
            }
        }

        private void buttonLogout_Click(object sender, EventArgs e) // ADMIN LOG OUT FUNCTION
        {
            Hide();                                
            LoginPage login = new LoginPage();     
            login.ShowDialog();                   
            this.Close();                         
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            adminListView.Items.Clear();
            loadAdminTimetable();
            checkBox1.Checked = false;
        }

        private void button2_Click(object sender, EventArgs e) // Delete  Appointment button        Time Date InstructorName Client
        {
            string IdD = "";


            foreach (ListViewItem eachItem in adminListView.SelectedItems)
            {
                IdD = adminListView.SelectedItems[0].Text;

                // label14.Text = carid; // to test if variable is passed through

                SQL.executeQuery($"Delete From Timeslot Where Id = '{IdD}'"); //sql query 
                adminListView.Items.Remove(eachItem); // deletes item from listview

            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            adminListView.Items.Clear();
            string date = dateTimePicker1.Text.ToString();
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, Status, Timeoff, ClientType FROM TimeSlot WHERE Date = '{date}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                adminListView.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["ClientType"].ToString());
                adminListView.Items.Add(item);
            }
            con.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            adminListView.Items.Clear();
            string instructor = comboBoxFilterInstructor.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, Status, Timeoff, ClientType FROM Timeslot WHERE InstructorName = '{instructor}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                adminListView.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["ClientType"].ToString());
                adminListView.Items.Add(item);
            }
            con.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            adminListView.Items.Clear();
            string client = comboBoxFilterClient.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, Status, Timeoff, ClientType FROM TimeSlot WHERE Client = '{client}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                adminListView.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["ClientType"].ToString());
                adminListView.Items.Add(item);
            }
            con.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            adminListView.Items.Clear();
            string date = dateTimePicker1.Text.ToString();
            string instructor = comboBoxFilterInstructor.Text;
            string client = comboBoxFilterClient.Text;
            con.Open();
            cmd.CommandText = $"SELECT Id, Time, Date, InstructorName, Client, Status, Timeoff, ClientType FROM TimeSlot WHERE Date = '{date}' AND InstructorName = '{instructor}' AND Client = '{client}'";
            cmd.Connection = con;
            read1 = cmd.ExecuteReader();
            while (read1.Read())
            {
                adminListView.View = View.Details;
                ListViewItem item = new ListViewItem(read1["Id"].ToString());
                item.SubItems.Add(read1["Date"].ToString());
                item.SubItems.Add(read1["Time"].ToString());
                item.SubItems.Add(read1["InstructorName"].ToString());
                item.SubItems.Add(read1["Client"].ToString());
                item.SubItems.Add(read1["Status"].ToString());
                item.SubItems.Add(read1["Timeoff"].ToString());
                item.SubItems.Add(read1["ClientType"].ToString());
                adminListView.Items.Add(item);
            }
            con.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            comboBoxFilterInstructor.SelectedItem = null;
            comboBoxFilterClient.SelectedItem = null;
        }

        private void button12_Click(object sender, EventArgs e)  // Delete Users Button
        {
            DeleteUsers delete = new DeleteUsers();
            //show the register page
            delete.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)  // Admin Register Button
        {
            AdminRegisterPage register = new AdminRegisterPage();
            register.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)  // Instructor Register Button
        {
            InstructorRegisterPage register = new InstructorRegisterPage();
            register.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e) // Client Register Button
        {
            RegisterPage register = new RegisterPage();
            //show the register page
            register.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e) // View Statistics button
        {
            StatisticsPage statistics = new StatisticsPage();
            //show the register page
            statistics.ShowDialog();
        }

        private void button15_Click(object sender, EventArgs e) // Approve Instructor Blockout
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to confirm this Timeoff?", "Warning!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string id = "";
                string timeoff;

                foreach (ListViewItem eachItem in adminListView.SelectedItems)
                {
                    id = adminListView.SelectedItems[0].Text;
                    timeoff = adminListView.SelectedItems[0].SubItems[6].Text;
                    if (timeoff == "Requested")
                    {
                        SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'Confirmed' WHERE Id = '{id}' AND Timeoff is NOT NULL"); //sql query
                        adminListView.Items.Clear();
                        loadAdminTimetable();
                    }
                    else if (timeoff == "Confirmed")
                    {
                        MessageBox.Show($"Sorry, this timeoff has already been confirmed.");
                    }
                    else if (timeoff == "Declined")
                    {
                        DialogResult dialog = MessageBox.Show("This timeoff request has already been declined, are you sure you still want to confirm this Timeoff?", "Warning!", MessageBoxButtons.YesNo);
                        if (dialog == DialogResult.Yes)
                        {
                            SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'confirmed' WHERE Id = '{id}' AND Timeoff is NOT NULL"); //sql query
                            adminListView.Items.Clear();
                            loadAdminTimetable();
                            break;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"Sorry, this is a appointment slot not a timeoff slot.");
                    }
                }
            }
        }

        private void button14_Click(object sender, EventArgs e) // Confirm Bill Payment
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to confirm this bill as paid?", "Warning!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string IdD = "";
                string Status = "";
                foreach (ListViewItem eachItem in adminListView.SelectedItems)
                {
                    IdD = adminListView.SelectedItems[0].Text;
                    Status = adminListView.SelectedItems[0].SubItems[5].Text;
                    if (Status == "Completed")
                    {
                        SQL.executeQuery($"UPDATE Timeslot SET Status = 'Paid' WHERE Id = '{IdD}'"); //sql query
                        adminListView.Items.Clear();
                        loadAdminTimetable();
                    }
                    else if (Status == "Incomplete")
                    {
                        MessageBox.Show($"Sorry, this appointment hasn't been completed yet.");
                    }
                    else if (Status == "Paid")
                    {
                        MessageBox.Show($"Sorry, the bill for this appointment has already been paid.");
                    }
                    else
                    {
                        MessageBox.Show($"Sorry, this is a timeoff slot not an appointment.");
                    }
                }
            }
        }

        private void button16_Click(object sender, EventArgs e) // Decline Instructor Blockout
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to decline this timeoff?", "Warning!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string id = "";
                string timeoff;

                foreach (ListViewItem eachItem in adminListView.SelectedItems)
                {
                    id = adminListView.SelectedItems[0].Text;
                    timeoff = adminListView.SelectedItems[0].SubItems[6].Text;

                    if (timeoff == "Requested")
                    {
                        SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'Declined' WHERE Id = '{id}' AND Timeoff is NOT NULL"); //sql query
                        adminListView.Items.Clear();
                        loadAdminTimetable();
                    }
                    else if (timeoff == "Declined")
                    {
                        MessageBox.Show($"Sorry, this timeoff has already been declined.");
                    }
                    else if (timeoff == "Confirmed")
                    {
                        DialogResult dialog = MessageBox.Show("This timeoff has already been confirmed, are you sure you still want to decline it?", "Warning!", MessageBoxButtons.YesNo);
                        if (dialog == DialogResult.Yes)
                        {
                            SQL.executeQuery($"UPDATE Timeslot SET Timeoff = 'declined' WHERE Id = '{id}' AND Timeoff is NOT NULL"); //sql query
                            adminListView.Items.Clear();
                            loadAdminTimetable();
                            break;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"Sorry, this is an appointment slot not a timeoff request.");
                    }
                }
            }
        }
    }
   }

