﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace INTRO_USERS
{
    public partial class ClientBooking : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS;Integrated Security=True");
        //SqlConnection con = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True");

        SqlCommand cmd = new SqlCommand();
        SqlDataReader read; 

        public string name = "";

        public string firstname1 = "";
        public string lastname1 = "";
        public string type = "";

        /// <summary>
        /// //This BrowsePage method executes upon loading of the form
        /// </summary>
        /// 


        public ClientBooking(string CurrentUser, string firstname, string lastname)
        {
            InitializeComponent();

            label9.Text = CurrentUser;

            name = CurrentUser;



            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))
            //using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True"))       
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FirstName, LastName FROM Instructor where FirstName != 'Spare'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();

                while (sqlReader.Read())
                {
                    comboBox1.Items.Add(sqlReader["FirstName"].ToString() + " " + (sqlReader["LastName"].ToString()));
                }
                sqlReader.Close();

                SqlCommand sqlCmd2 = new SqlCommand($"SELECT Type FROM Client WHERE Name = '{CurrentUser}'", sqlConnection);
                SqlDataReader sqlReader2 = sqlCmd2.ExecuteReader();
                while (sqlReader2.Read())
                {
                    type = (sqlReader2["Type"].ToString());
                }
                sqlReader2.Close();
            }

            
        }

        
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //You should have seen this from the register page, same code to switch forms.
            Hide();
            LoginPage login = new LoginPage();
            login.ShowDialog();
            Close();
        }

        /// <summary>
        /// Gets the social media id based on the social media name
        /// </summary>
        /// <param name="socialMedia">The name of the social media from combo box</param>
        /// <returns>The ID of the social media from database, blank string returned if not in database</returns>


        private void BrowsePage_Load(object sender, EventArgs e)
        {
            ResetColor();
        }

        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string client;
        
        
        public void button1_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "7:00am-8:00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button1.BackColor = Color.Green;
        }

        public void button8_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "9:00am-10:00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button8.BackColor = Color.Green;
        }

        public void button3_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "10:00am-11:00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button3.BackColor = Color.Green;
        }

        public void button10_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "11:00am-12:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button10.BackColor = Color.Green;
        }

        public void button6_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "12:00pm-1:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button6.BackColor = Color.Green;
        }

        public void button5_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "1:00pm-2:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button5.BackColor = Color.Green;
        }

        public void button4_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "2:00pm-3:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button4.BackColor = Color.Green;
        }

        public void button11_Click(object sender, EventArgs e)
        {
            ResetColor();
            timeslot = "3:00pm-4:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button11.BackColor = Color.Green;
        }

        public void button7_Click(object sender, EventArgs e)
        {
            ResetColor();

            timeslot = "4:00pm-5:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button7.BackColor = Color.Green;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ResetColor();

            timeslot = "7:00pm-8:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button12.BackColor = Color.Green;
        }
        private void button14_Click(object sender, EventArgs e)
        {
            ResetColor();

            timeslot = "6:00pm-7:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button14.BackColor = Color.Green;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            ResetColor();

            timeslot = "5:00pm-6:00pm";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button15.BackColor = Color.Green;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            ResetColor();

            timeslot = "8:00am-9:00am";
            label7.Text = dateTimePicker1.Text.ToString() + " From: " + timeslot;
            button16.BackColor = Color.Green;
        }

        public void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            ResetColor();

            string Date = "", selectDay = "";
            Date = dateTimePicker1.Text.ToString();
            selectDay = Date;

            if (selectDay[0] == 'S' && selectDay[1] == 'u')
            {
                button1.Visible = false;
                button3.Visible = false;
                button6.Visible = false;
                button8.Visible = false;
                button11.Visible = false;
                button10.Visible = false;
                button7.Visible = false;
                button5.Visible = false;
                button4.Visible = false;
                button12.Visible = false;
                button14.Visible = false;
                button15.Visible = false;
                button16.Visible = false;

                labelSunday.Visible = true;               
            }
            else
            {
                button1.Visible = true;
                button3.Visible = true;
                button6.Visible = true;
                button8.Visible = true;
                button11.Visible = true;
                button10.Visible = true;
                button7.Visible = true;
                button5.Visible = true;
                button4.Visible = true;
                button12.Visible = true;
                button14.Visible = true;
                button15.Visible = true;
                button16.Visible = true;
                labelSunday.Visible = false;
            }

        }
        public void  ResetColor()
        {
            button1.BackColor = Color.FromKnownColor(KnownColor.Control);
            button3.BackColor = Color.FromKnownColor(KnownColor.Control);
            button6.BackColor = Color.FromKnownColor(KnownColor.Control);
            button8.BackColor = Color.FromKnownColor(KnownColor.Control);
            button11.BackColor = Color.FromKnownColor(KnownColor.Control);
            button10.BackColor = Color.FromKnownColor(KnownColor.Control);
            button7.BackColor = Color.FromKnownColor(KnownColor.Control);
            button5.BackColor = Color.FromKnownColor(KnownColor.Control);
            button4.BackColor = Color.FromKnownColor(KnownColor.Control);
            button9.BackColor = Color.FromKnownColor(KnownColor.Control);
            button13.BackColor = Color.FromKnownColor(KnownColor.Control);
            button12.BackColor = Color.FromKnownColor(KnownColor.Control);
            button14.BackColor = Color.FromKnownColor(KnownColor.Control);
            button15.BackColor = Color.FromKnownColor(KnownColor.Control);
            button16.BackColor = Color.FromKnownColor(KnownColor.Control);
        } 

        public void button13_Click(object sender, EventArgs e) //submit button ------------------
        {
            //variables to be used
            string Time = "", Date = "", InstructorName = "", CurrentUser = "", email = "";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            InstructorName = comboBox1.Text;
            CurrentUser = label9.Text;

            string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            TimeI = Time;
            DateI = Date;
            InstructorNameI = InstructorName;
            CurrentUserI = CurrentUser;

            if (comboBox1.SelectedItem == null || label7.Text == "")
            {
                MessageBox.Show("Error, Missing instructor or time/date.");

            }
            else
            { // TEST CODE STARTS ------------------------------------------------------------------------

                using (SqlConnection sqlConnection = new SqlConnection(@"Data Source = localhost\; Database = INTRO_USERS; Integrated Security = True"))   
                //using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=TYLER-PC\SQLEXPRESS;Database=INTRO_USERS;Integrated Security=True"))
                {
                    // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                    SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}'", sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                    while (sqlReader.Read())
                    {
                        if (sqlReader.HasRows)
                        {
                            TimeI = sqlReader.GetString(1);
                            DateI = sqlReader.GetString(2);
                            InstructorNameI = sqlReader.GetString(3);
                            // CurrentUserI = sqlReader.GetString(4);


                            if (TimeI == "" && DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                            {

                                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, ClientType) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "', 'Incomplete', '" + type + "')");
                                //success message for the user to know it worked
                                MessageBox.Show("Successful - Your appointment is: " + Date + " at " + Time + " With " + InstructorName); 


                            }
                            else
                            {
                                MessageBox.Show("Time unavalible. Please Pick Another");
                                return;
                            }
                        }

                    }

                    sqlReader.Close();
                }
                // CheckInput(); TEST CODE ENDS ---------------------------------------------------------------
               try
                {


                    SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, ClientType) VALUES ('" + Time + "', '" + Date + "','" + InstructorName + "', '" + CurrentUser + "', 'Incomplete', '" + type + "')");
                    //success message for the user to know it worked
                    MessageBox.Show("Successful - Your appointment is: " + Date + " at " + Time + " With " + InstructorName);


                    // Email Appointment Confirmation 
                    //var command = ($"SELECT email from Client where firstname + lastname = '{firstname}' + '{lastname}'");

                    // SQL.executeQuery($"SELECT email from Client where firstname + lastname = '{firstname}' + '{lastname}'");
                    //email =(string)command.ExecuteScalar();

                    
                    //SqlCommand cmd = new SqlCommand($"SELECT email from Client where firstname + lastname = '{firstname}' + '{lastname}'");
                    con.Open();
                    cmd.CommandText = ($"SELECT email from Client where name = '{CurrentUser}'");
                    cmd.Connection = con;
                    read = cmd.ExecuteReader();

                    while (read.Read())
                    {

                       // ListViewItem item = new ListViewItem(read["Id"].ToString());

                        email = read[0].ToString();

                    }
                    con.Close();

                   // MessageBox.Show("hello" + " - " + email + " - " + firstname1 + " - " + lastname1); 

                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.Credentials = new System.Net.NetworkCredential("diaemailservice@gmail.com", "database");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                                    
                    mail.From = new MailAddress("9997861@bcs.net.nz",
                    "DIA - Driving Instruction Academy", System.Text.Encoding.UTF8);
                    mail.To.Add(email); //mail.To.Add("9997861@bcs.net.nz");
                    mail.Subject = $"DIA - Appointment Notification - " + CurrentUser;
                    mail.Body = ("Successful - Your appointment is: " + Date + " at " + Time + " With " + InstructorName);
                    //mail.Attachments.Add(new Attachment(Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory), "invoices", $"{invoiceClient}-{slotID}.pdf")));
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    //mail.ReplyTo = new MailAddress("9997861@bcs.net.nz");
                    SmtpServer.Send(mail);
                    //SQL.executeQuery($"UPDATE Invoices SET invoiceStatus = 'Emailed' WHERE InvoiceID = '{invoiceID}'");
                    MessageBox.Show($"Email successfully sent to: " + email); 
                    
                }
                catch (Exception /* ex */)
                {
                   //MessageBox.Show(ex.ToString());
                   MessageBox.Show("Appointment Booked! Confirmation Email was not sent as Client Email was not valid. Contact DIA to resolve this issue. ");
                    return;
                } 

                
            }
        }

        private void button9_Click(object sender, EventArgs e) // BLOCK OUT FULL DAY
        {
            //variables to be used
            string Time = "", Date = "", InstructorName = "", LoggedInUser = "";

            Time = timeslot;
            Date = dateTimePicker1.Text.ToString();
            //InstructorName = comboBox1.Text;


            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "7:00am-8:00am" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "8:00am-9:00am" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "9:00am-10:00am" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "10:00am-11:00am" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "11:00am-12:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "12:00am-1:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "1:00pm-2:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "2:00pm-3:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "3:00pm-4:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "4:00pm-5:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "5:00pm-6:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "6:00pm-7:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");
                SQL.executeQuery("INSERT INTO Timeslot (Time, Date, InstructorName, Client) VALUES ('" + "7:00pm-8:00pm" + "', '" + Date + "', '" + InstructorName + "', '" + LoggedInUser + "' )");

            }
            catch (Exception)
            {
                MessageBox.Show("error");
                return;
            }

            //success message for the user to know it worked
            MessageBox.Show("Successfully blocked out whole day on: " + Date);

            //Go back to the login page since we registered successfully to let the user log in
            /*  Hide();                                 //hides the register form
             LoginPage login = new LoginPage();      //creates the login page as an object
             login.ShowDialog();                     //shows the new login page form
             this.Close();                           //closes the register form that was hidden */
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Hide();                                 //hides the register form
            LoginPage login = new LoginPage();      //creates the login page as an object
            login.ShowDialog();                     //shows the new login page form
            this.Close();                           //closes the register form that was hidden */
        }

        private void button17_Click(object sender, EventArgs e) // SHOW TIMETABLE BUTTON
        {
            ClientTimetable popup = new ClientTimetable();
            popup.ShowDialog();
        }


    }
}
