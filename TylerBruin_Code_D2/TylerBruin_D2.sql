/*
CREATE DATABASE INTRO_USERS
GO

USE INTRO_USERS
GO 

Tyler Bruin - 9997861
*/
-- Create Tables
CREATE TABLE Admin (
	UserName varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	email varchar(50) NOT NULL
	)

CREATE TABLE Type (
	Name varchar(50) NOT NULL PRIMARY KEY,
	Cost varchar(20) NOT NULL,
	Hours int NOT NULL
	)

CREATE TABLE Document (
	ID int NOT NULL PRIMARY KEY,
	Date date NOT NULL,
	Link varchar(50) NOT NULL,
	Type varchar(50) NOT NULL
	)

CREATE TABLE Instructor (
	Username varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	phone varchar(50) NOT NULL
	)

CREATE TABLE Car (
	License varchar(50) NOT NULL PRIMARY KEY,
	Make varchar(50) NOT NULL
	)

CREATE TABLE Appointment (
	ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Notes varchar(2000)
	)

CREATE TABLE Client (
	Username varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	name varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	phone varchar(50) NOT NULL,
	Type varchar(50) NOT Null,
	FOREIGN KEY (Type) REFERENCES Type
	)

CREATE TABLE Timeslot (
	Id int IDENTITY(1,1) NOT NULL,
	Time varchar(50) NOT NULL,			
	Date varchar(100) NOT NULL,
	InstructorName varchar(50) NOT NULL, 
	Client varchar(50),
	Status varchar(50),
	Timeoff varchar(50),
	ClientType varchar(50),
	PRIMARY KEY (Id, Time, Date),
	)
		

CREATE TABLE Books (
	Client_Username varchar(50) NOT NULL,
	Appointment_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Appointment_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Appointment_Id) REFERENCES Appointment
	)

CREATE TABLE Receives (
	Client_Username varchar(50) NOT NULL,
	Doc_Id int NOT NULL,
	PRIMARY KEY (Client_Username, Doc_Id),
	FOREIGN KEY (Client_Username) REFERENCES Client,
	FOREIGN KEY (Doc_Id) REFERENCES Document
	)

CREATE TABLE Assigned (
	License varchar(50) NOT NULL,
	InstructorName varchar(50) NOT NULL,
	PRIMARY KEY (License, InstructorName),
	--FOREIGN KEY (License) REFERENCES Car,
	--FOREIGN KEY (InstructorName) REFERENCES Instructor,
	)

CREATE TABLE Users (
    Username varchar(20) NOT NULL PRIMARY KEY,
    FirstName varchar(20) NOT NULL,
    LastName varchar(20) NOT NULL,
    Password varchar(30) NOT NULL,
    Email varchar(50) NOT NULL,
    Phone varchar(20),
    CourseType varchar(30),
    Status varchar(20) NOT NULL
    )


-- Drop statements
/*
DROP TABLE Admin
DROP TABLE Type
DROP TABLE Document
DROP TABLE Appointment
DROP TABLE Car
DROP TABLE Instructor
DROP TABLE Timeslot
DROP TABLE Client
DROP TABLE Books
DROP TABLE Receives
DROP TABLE Assigned
DROP TABLE Users  
*/

--Insert statements
INSERT INTO Admin VALUES ('a', 'a', 'a', 'a', 'a')
INSERT INTO Admin VALUES ('Adam', 'Adam', 'Adam', 'Hamilton', '9997861@bcs.net.nz')
INSERT INTO Admin VALUES ('Alex', 'Alex', 'Alex', 'Wilson', '9997861@bcs.net.nz')

INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('a', 'a', 'a', 'a', 'a','a',NULL,'admin')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Adam', 'Adam', 'Adam', 'Hamilton', '9997861@bcs.net.nz','0271234567',NULL,'admin')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Alex', 'Alex', 'Alex', 'Wilson', '9997861@bcs.net.nz','0271234567',NULL,'admin')

INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Paul', 'Paul', 'Paul', 'Jackson', 'paul@gmail.com', '0271112222',NULL,'instructor')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Jack', 'Jack', 'Jack', 'Paterson', 'jack@gmail.com', '0273334444',NULL,'instructor')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Jeff', 'Jeff', 'Jeff', 'Jones', 'jeff@gmail.com', '0275556666',NULL,'instructor')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Ryan', 'Ryan', 'Ryan', 'Patt', 'ryan@gmail.com', '0277778888',NULL,'instructor')

INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Cory', 'Cory', 'Cory', 'Bruce', '9997861@bcs.net.nz', '0277778888','Intermediate','client')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Craig', 'Craig', 'Craig', 'Jones', '9997861@bcs.net.nz', '0277778888','Beginner','client')
INSERT INTO Users (Username, Password, FirstName, LastName, Email, Phone, CourseType, Status) VALUES ('Colin', 'Colin', 'Colin', 'Walker', '9997861@bcs.net.nz', '0277778888','Advanced','client')


INSERT INTO Type VALUES ('Beginner', '$200', 10)
INSERT INTO Type VALUES ('Intermediate','$150', 7)
INSERT INTO Type VALUES ('Advanced', '$100', 5)

INSERT INTO Instructor VALUES ('Spare', 'Spare', 'Spare', 'Vehicle', 'spare', 'spare')
INSERT INTO Instructor VALUES ('Paul', 'Paul', 'Paul', 'Jackson', 'paul@gmail.com', '0271112222')
INSERT INTO Instructor VALUES ('Jack', 'Jack', 'Jack', 'Paterson', 'jack@gmail.com', '0273334444')
INSERT INTO Instructor VALUES ('Jeff', 'Jeff', 'Jeff', 'Jones', 'jeff@gmail.com', '0275556666')
INSERT INTO Instructor VALUES ('Ryan', 'Ryan', 'Ryan', 'Patt', 'ryan@gmail.com', '0277778888')


INSERT INTO Car VALUES ('C4R111', 'Subaru')
INSERT INTO Car VALUES ('C4R222', 'Nissan')
INSERT INTO Car VALUES ('C4R333', 'Toyota')
INSERT INTO Car VALUES ('C4R444', 'Holden')
INSERT INTO Car VALUES ('C4R555', 'lamborghini')

INSERT INTO Client VALUES ('Cory', 'Cory', 'Cory Bruce', '9997861@bcs.net.nz', '0277778888', 'Intermediate')
INSERT INTO Client VALUES ('Craig', 'Craig', 'Craig Jones', '9997861@bcs.net.nz', '0277778888', 'Beginner')
INSERT INTO Client VALUES ('Colin', 'Colin', 'Colin Walker', '9997861@bcs.net.nz', '0277778888', 'Advanced')


/* ----------------------------- Client Bookings--------------------- */

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('7:00pm-8:00pm', 'Friday, 2 June 2017', 'Jack Paterson', 'Craig Jones', 'Paid', NULL, 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Friday, 2 June 2017', 'Jack Paterson', 'Cory Bruce', 'Paid', NULL, 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Friday, 2 June 2017', 'Ryan Patt', 'Colin Walker', 'Paid', NULL, 'Advanced')

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('7:00pm-8:00pm', 'Monday, 12 June 2017', 'Jack Paterson', 'Craig Jones', 'Completed', NULL, 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Monday, 12 June 2017', 'Jack Paterson', 'Cory Bruce', 'Completed', NULL, 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Monday, 12 June 2017', 'Ryan Patt', 'Colin Walker', 'Completed', NULL, 'Advanced')


INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('7:00pm-8:00pm', 'Thursday, 22 June 2017', 'Paul Jackson', 'Craig Jones', 'Incomplete', NULL, 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('7:00pm-8:00pm', 'Friday, 23 June 2017', 'Paul Jackson', 'Craig Jones', 'Incomplete', NULL, 'Beginner')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('7:00pm-8:00pm', 'Saturday, 24 June 2017', 'Paul Jackson', 'Craig Jones', 'Incomplete', NULL, 'Beginner')

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Wednesday, 14 June 2017', 'Jeff Jones', 'Cory Bruce', 'Incomplete', NULL, 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Thursday, 16 June 2017', 'Jeff Jones', 'Cory Bruce', 'Incomplete', NULL, 'Intermediate')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Friday, 16 June 2017', 'Jeff Jones', 'Cory Bruce', 'Incomplete', NULL, 'Intermediate')

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Thursday, 15 June 2017', 'Ryan Patt', 'Colin Walker', 'Incomplete', NULL, 'Advanced')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Friday, 16 June 2017', 'Ryan Patt', 'Colin Walker', 'Incomplete', NULL, 'Advanced')
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('8:00am-9:00am', 'Saturday, 17 June 2017', 'Ryan Patt', 'Colin Walker', 'Incomplete', NULL, 'Advanced')


/* ----------------------------- Instructor Bookings --------------------- */

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('12:00pm-1:00pm', 'Saturday, 24 June 2017', 'Ryan Patt', NULL, NULL, 'Confirmed', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('12:00pm-1:00pm', 'Monday, 26 June 2017', 'Ryan Patt', NULL, NULL, 'Confirmed', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('12:00pm-1:00pm', 'Saturday, 27 June 2017', 'Ryan Patt', NULL, NULL, 'Confirmed', NULL)

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('10:00am-11:00pm', 'Tuesday, 27 June 2017', 'Paul Jackson', NULL, NULL, 'Declined', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('10:00am-11:00pm', 'Wednesday, 28 June 2017', 'Paul Jackson', NULL, NULL, 'Declined', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('10:00am-11:00pm', 'Thursday, 29 June 2017', 'Paul Jackson', NULL, NULL, 'Declined', NULL)

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('10:00am-11:00pm', 'Thursday, 29 June 2017', 'Jack Paterson', NULL, NULL, 'Confirmed', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('10:00am-11:00pm', 'Friday, 30 June 2017', 'Jack Paterson', NULL, NULL, 'Confirmed', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('10:00am-11:00pm', 'Saturday, 1 July 2017', 'Jack Paterson', NULL, NULL, 'Confirmed', NULL)

INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('6:00pm-7:00pm', 'Tuesday, 27 June 2017', 'Jeff Jones', NULL, NULL, 'Requested', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('6:00pm-7:00pm', 'Wednesday, 28 June 2017', 'Jeff Jones', NULL, NULL, 'Requested', NULL)
INSERT INTO Timeslot (Time, Date, InstructorName, Client, Status, TimeOff, ClientType) VALUES ('6:00pm-7:00pm', 'Thursday, 29 June 2017', 'Jeff Jones', NULL, NULL, 'Requested', NULL)


-- Select Statements
/*
Select * FROM Admin
Select * FROM Type
Select * FROM Instructor
Select * FROM Car
Select * FROM Appointment
Select * FROM Timeslot

Select FirstName, LastName FROM Instructor

Select * FROM Client

Select * FROM Document
Select * FROM Books
Select * FROM Receives
Select * FROM Assigned 
Select * FROM Users

SELECT username FROM Users WHERE Status = 'admin' OR Status = 'instructor'



*/
/*
Select count(*) From Users
Select count(*) From Timeslot Where timeoff = 'Requested'  

Select count(*) From Client where Type = 'Beginner'
Select count(*) From Client where Type = 'Intermediate'
Select count(*) From Client where Type = 'Advanced'



SELECT *
FROM Timeslot
WHERE MONTH(date) = MONTH(CURRENT_DATE())

Select getdate()

Select count(*) from timeslot where date like '%June%'
Select count(*) from timeslot where date like '%June%'
 
select *
from timeslot
where MONTH(date) = MONTH(getdate())
and MONTH(date) = YEAR(getdate())

SELECT email from Client where firstname + lastname = 'tyler bruin'
SELECT email from Client where firstname LIKE '%tyler'
SELECT email from Client where 'tyler bruin' = firstname + lastname

SELECT firstname + lastname as Name from Client 
SELECT FirstName, LastName FROM Instructor where FirstName != 'Spare'

SELECT count(InstructorName) As Total, InstructorName 
From Timeslot Where Client != 'Blocked Out'
Group By InstructorName 

Select count(*) From Instructor Where Firstname NOT LIKE 'Spare%'
Select * From Instructor Where Firstname NOT LIKE 'Spare%'

Select count(*) from timeslot where date like '%June%' and Client != 'Blocked Out'
Select count(*) from timeslot where Client != 'Blocked Out'
Select * From Timeslot where Client != 'Blocked Out'

*/